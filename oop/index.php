<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

// Release 0

$animal = new animal("shaun");
echo "Name : $animal->name <br>";
echo "Legs : $animal->legs <br>";
echo "Cold Blooded : ";
echo var_dump($animal->cold_bloded);
echo "<br><br>";

// Release 1
$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Yel : ";
$sungokong->yell() ; // "Auooo"
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Jump : ";
$kodok->jump() ; // "hop hop"
echo "<br><br>";

?>